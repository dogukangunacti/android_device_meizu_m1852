#
# Copyright (C) 2020 The MoKee Open Source Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/evolution_m1852.mk

COMMON_LUNCH_CHOICES := \
    evolution_m1852-userdebug
